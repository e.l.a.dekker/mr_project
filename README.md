# EsTAn: A Content-Based 3D Shape Retrieval System

An unsupervised, content-based 3D shape retrieval pipeline. Tool that is capable of receiving a query shape and extracting the most similar shapes in a given database.

## Requirements

You first need to have a Python 3.6 version in order to run the program, higher versions are not supported because of some unsupported libraries.

In order to install all the necessary packages you need to run:
```
$ pip install -r requirements.txt
```

## Usage

First, you need to open a Termina(CMD) where the Project is located, or use some Python IDE.

To start the system you need to start the scr/gui.py in your IDE or run a Python start command.

In the newly created GUI, you can select a file of the shape you want to query and the program will return the 6 most silimar shapes. 

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
