import trimesh
from matplotlib import pyplot as plt
from view_example import *
from statistics import median, mean
from calculate_feature_vector import read_feature_table

def load_shapes():
    """
    Load all meshes from the DB.
    """
    list_shapes = []
    for i in range(19):  # type of shape
        counter = 0 + i * 100
        for j in range(100):
            if i == 18 and j == 15:
                break
            if counter not in error_shapes:
                shape = trimesh.load_mesh('../data/db/{}/m{n}/m{n}.off'.format(i, n=counter))
                list_shapes.append(shape)
                print("Read mesh", counter)
            counter += 1
    return list_shapes


def load_labeled_db(refine=False):
    """
    Load all meshes from the labeled PSB database.
    """
    list_shapes = []
    if refine:
        file_name = '../output/fixed_watertightness'
        # file_name = '../output/preprocessed_meshes'
    else:
        file_name = '../labeled_PSB_data'

    for k, v in labels.items():  # list(labels.items())[0:1]:  # type of shape
        counter = v
        for j in range(20):
            shape = trimesh.load('{file}/{c}/{n}.off'.format(file=file_name, c=k, n=counter), process=False)
            list_shapes.append(shape)
            print("Read mesh", k, counter)
            counter += 1
    return list_shapes


def create_histogram_faces(shape_list, status_val):
    """
    Create a histogram of the frequency of the number of faces in a mesh.

    :param shape_list: list of Shapes
    :param status_val: bool value
    """
    if status_val:
        status = "after"
        name = "refined_"
    else:
        status = "before"
        name = ""
    list_faces_count = [x.faces.shape[0] for x in shape_list]
    print("Faces mean:", mean(list_faces_count))
    print("Faces median:", median(list_faces_count))
    plt.clf()  # clear histogram
    plt.hist(list_faces_count, bins=25)
    plt.title("Frequency of the number of faces {c} remeshing".format(c=status))
    plt.xlabel("# Faces")
    plt.ylabel("Frequency")
    plt.savefig("../output/histograms/hist_{t}faces.png".format(t=name))


def create_histogram_vertices(shape_list, status_val):
    """
    Create a histogram of the frequency of the number of vertices in a mesh.

    :param shape_list: list of Shapes
        :param status_val: bool value
    """
    if status_val:
        status = "after"
        name = "refined_"
    else:
        status = "before"
        name = ""
    list_vertices_count = [x.vertices.shape[0] for x in shape_list]
    print("Vertices mean:", mean(list_vertices_count))
    print("Vertices median:", median(list_vertices_count))
    plt.clf()  # clear histogram
    plt.hist(list_vertices_count, bins=25)
    plt.title("Frequency of the number of vertices {c} remeshing".format(c=status))
    plt.xlabel("# Vertices")
    plt.ylabel("Frequency")
    plt.savefig("../output/histograms/hist_{t}vertices.png".format(t=name))


def create_histogram_scale(shape_list, status_val):
    if status_val:
        status = "after"
        name = "remeshed_"
    else:
        status = "before"
        name = ""
    list_vertices_count = [x.scale for x in shape_list]
    plt.clf()  # clear histogram
    f, ax = plt.subplots()
    ax.set_ylim([0.0, 120.0])
    ax.hist(list_vertices_count, bins=25, range=(0.0, 3.0))
    plt.title("Scale of shape {c} normalization".format(c=status))
    plt.xlabel("Scale")
    plt.ylabel("Frequency")
    plt.savefig("../output/histograms/hist_scaled_{t}shapes.png".format(t=name))


def create_histogram_centering(shape_list, status_val):
    if status_val:
        status = "after"
        name = "remeshed_"
    else:
        status = "before"
        name = ""
    center = [0, 0, 0]
    list_vertices_count = [np.linalg.norm(x.centroid - center) for x in shape_list]
    plt.clf()  # clear histogram
    f, ax = plt.subplots()
    ax.set_ylim([0.0, 300.0])
    ax.hist(list_vertices_count, bins=25, range=(0.0, 0.5))
    plt.title("Distances between the shapes and the origin {c} centering".format(c=status))
    plt.xlabel("Distance")
    plt.ylabel("Frequency")
    plt.savefig("../output/histograms/hist_{t}shapes_distance2origin.png".format(t=name))


def create_polyline_hist():
    features = read_feature_table()
    x1 = [0., 18.,  36.,  54.,  72.,  90., 108., 126., 144., 162.]
    x2 = [0., 0.17320508, 0.34641016, 0.51961524, 0.69282032, 0.8660254, 1.03923048, 1.21243557, 1.38564065, 1.55884573]
    x3 = [0., 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

    for curr_class in list(labels.keys()):
        f = [i for i in features if i.cid == curr_class]
        plot_polyline_hist([e.A3 for e in f], curr_class, "A3", x1, 180., -10, "Angles")
        plot_polyline_hist([e.D1 for e in f], curr_class, "D1", x2,  1.73205081, -0.1, "Distances")
        plot_polyline_hist([e.D2 for e in f], curr_class, "D2", x2,  1.73205081, -0.1, "Distances")
        plot_polyline_hist([e.D3 for e in f], curr_class, "D3", x3, 1., -0.05, "Square root of the area of triangles")
        plot_polyline_hist([e.D4 for e in f], curr_class, "D4", x3, 1., -0.05, "Cube root of volume of tetrahedron")


def plot_polyline_hist(ys, class_name, fea, xas_labels, xas, min_x, name_xas):
    # y = np.array([0.156, 0.524, 0.32, 0., 0., 0., 0., 0., 0., 0.])
    print("class:", class_name, fea)
    plt.clf()  # clear histogram
    plt.ylim(0.0, 1.0)
    plt.xlim(min_x, xas)
    for g in ys:
        plt.plot(xas_labels, g)
    plt.title("Polyline graph of {f} for class: {c}".format(f=fea, c=class_name))
    plt.xlabel(name_xas)
    plt.ylabel("Frequency")
    plt.savefig("../output/polyline_graphs/pl_{t}_{p}.png".format(t=class_name, p=fea))


def create_histogram_alignment(shape_list, status_val):
    if status_val:
        status = "after"
        name = "remeshed_"
    else:
        status = "before"
        name = ""
    list_vertices_count = []
    for shape in shape_list:
        cov_matrix = np.cov(shape.vertices.T).T
        eigenvalues, eigenvectors = np.linalg.eig(cov_matrix)

        idx = eigenvalues.argsort()[::-1]
        eigenvectors = eigenvectors[idx]
        list_vertices_count.append(np.dot(eigenvectors[0], [1, 0, 0]))

    plt.clf()  # clear histogram
    f, ax = plt.subplots()
    ax.set_ylim([0.0, 150.0])
    ax.hist(list_vertices_count, bins=25)  # , range=(0.0, 3.0))
    plt.title("Alignment of shape {c} normalization".format(c=status))
    plt.xlabel("Alignment of the major eigenvector and x-axis")
    plt.ylabel("Frequency")
    plt.savefig("../output/histograms/hist_aligned_{t}shapes.png".format(t=name))


if __name__ == "__main__":
    # print("Started loading meshes..")
    refine_norm = True
    s = load_labeled_db(refine=refine_norm)

    # print("Done! Beginning histogram generation..")
    # create_histogram_faces(s, refine_norm)
    # create_histogram_vertices(s, refine_norm)
    # create_histogram_centering(s, refine_norm)
    # create_histogram_scale(s, refine_norm)
    # create_polyline_hist()
    create_histogram_alignment(s, refine_norm)
    # print("All done!")
