from calculate_feature_vector import read_feature_table, normalize_standardization
import numpy as np
from tsne_python.tsne import tsne
import matplotlib.pyplot as plt
from matplotlib import patches as mp
from matplotlib.widgets import Button
from matplotlib.text import Annotation
import seaborn as sns
import matplotlib as mpl
mpl.use('Qt5Agg')

#import plotly.graph_objects as go


labels = {"Human":(0,0,0), "Cup":(0.933,0.604,0.302), "Glasses":(1,0,0), "Airplane":(0,1,0), "Ant":(0,0,1), "Chair":(1,1,0), "Octopus":(0,1,1),
                "Table":(1,0,1), "Teddy":(0.5,0.5,0.5), "Hand":(0.5,0,0), "Plier":(0,0.5,0), "Fish":(0,0,0.5), "Bird":(0,0.5,0.5), "Armadillo":(0.5,0,0.5),
                "Bust":(0.5,0.5,0), "Mech":(0.294,0,0.6), "Bearing":(1,0.078,0.576), "Vase":(0,1,0.5), "FourLeg":(0.941,0.902,0.549)}


if __name__ == "__main__":
    mpl.use('Qt5Agg')
    norm = plt.Normalize(1, 4)
    cmap = plt.cm.RdYlGn
    features = read_feature_table()
    all_features, mean_stdv, hist_mean_stdv = normalize_standardization(features)

    X = np.array([np.array(all_features[0].create_feature_vector())])
    classes = [labels[all_features[0].cid]]
    for f in all_features[1:]:
        X = np.vstack([X, np.array(f.create_feature_vector())])
        classes.append(labels[f.cid])
    classes = np.array(classes)

    Y = tsne(X, 2, 50, 30.0)
    recs = []
    ck = []
    for k, v in labels.items():
        recs.append(mp.Rectangle((0, 0), 1, 1, fc=v))
        ck.append(k)
    fig, ax = plt.subplots()
    #ax.scatter(Y[:, 0], Y[:, 1], c=classes)
    sc = plt.scatter(Y[:, 0], Y[:, 1], c=classes,cmap=cmap)

    c = classes[:,0]
    print(c)

    print(ck)

    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    ax.legend(handles=recs, labels=ck, title='Classes', loc='center left', bbox_to_anchor=(1, 0.5))

    annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                        bbox=dict(boxstyle="round", fc="w"),
                        arrowprops=dict(arrowstyle="->"))
    annot.set_visible(False)


    def update_annot(ind):

        pos = sc.get_offsets()[ind["ind"][0]]
        annot.xy = pos
        #text = "{}, {}".format(" ".join(list(map(str, ind["ind"]))),
         #                      " ".join([ck[n] for n in ind["ind"]]))
        text = "Human"
        for f in features[1:]:
            #print(f.mid)
            if f.mid == ind["ind"][0]:
                text = f.cid
        annot.set_text(text)
        annot.get_bbox_patch().set_facecolor(cmap(norm(c[ind["ind"][0]])))

        print(ind["ind"][0])
        print(c[ind["ind"][0]])
        print(cmap(norm(c[ind["ind"][0]])))
        annot.get_bbox_patch().set_alpha(0.4)


    def hover(event):
        vis = annot.get_visible()
        if event.inaxes == ax:
            cont, ind = sc.contains(event)
            if cont:
                update_annot(ind)
                annot.set_visible(True)
                fig.canvas.draw_idle()
            else:
                if vis:
                    annot.set_visible(False)
                    fig.canvas.draw_idle()


    fig.canvas.mpl_connect("motion_notify_event", hover)

    plt.show()
    fig.show()
