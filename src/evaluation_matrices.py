from calculate_feature_vector import *
from tabulate import tabulate
from itertools import product


def evaluation(distances, features):
    p = {"Human":[], "Cup":[], "Glasses":[], "Airplane":[], "Ant":[], "Chair":[], "Octopus":[], "Table":[], "Teddy":[],
         "Hand":[], "Plier":[], "Fish":[], "Bird":[], "Armadillo":[], "Bust":[], "Mech":[], "Bearing":[], "Vase":[],
         "FourLeg":[]}
    total_eval = []
    for i in range(len(distances)):
        shape_class = features[i].cid
        TP, FP, TN, FN = confusion_matrix(distances[i], shape_class)
        a = accuracy(TP, TN, len(distances)-1)
        pr = precision(TP, FP)
        r = recall(TP, FN)
        f1 = f1_score(pr, r)
        s = specificity(TN, FP)
        e = [a, pr, r, f1, s]
        p[shape_class].append(e)
        total_eval.append(e)
    print_evalution(p, total_eval)


def eval_p(distances, features, combi, n):
    total_eval = []
    for i in range(len(distances)):
        shape_class = features[i].cid
        TP, FP, TN, FN = confusion_matrix(distances[i], shape_class)
        pr = precision(TP, FP)
        total_eval.append(pr)
    eval = avg_class(total_eval)
    print(n, combi, "Total", eval)
    return [combi, eval]


def eval_all_combinations(all_features, glbl):
    all_combi = list(product(["euc", "cos", "emd"], repeat=5))
    percentages = []
    for r, b in enumerate(all_combi):
        dist = []
        for f in all_features:
            dist.append(grid_search(f, all_features, glbl, b))
        percentages.append(eval_p(dist, all_features, b, r))
    print("highest:", sorted(percentages, key=itemgetter(1))[-1])


def print_evalution(class_eval, t):
    data = []
    for k, v in class_eval.items():
        a = avg_class([i[0] for i in v])
        p = avg_class([i[1] for i in v])
        r = avg_class([i[2] for i in v])
        f1 = avg_class([i[3] for i in v])
        s = avg_class([i[4] for i in v])
        data.append([k, a, p, r, f1, s])

    data.append(["Total", avg_class([i[0] for i in t]),
                 avg_class([i[1] for i in t]),
                 avg_class([i[2] for i in t]),
                 avg_class([i[3] for i in t]),
                 avg_class([i[4] for i in t])])
    print()
    print(tabulate(data, headers=["Class", "Accuracy", "Precision", "Recall", "F1-score", "Specificity"]))

    # data = []
    # for k, v in class_eval.items():
    #     p = avg_class([i[1] for i in v])
    #     data.append([k, p])
    #
    # data.append(["Total", avg_class([i[1] for i in t])])
    # print(tabulate(data, headers=["Class", "Precision"]))
    # print()


def avg_class(values):
    return str(round(sum(values)/len(values)*100, 2))


def confusion_matrix(b, shape_class):
    tp = fp = 0
    for match in b[1:]:
        if shape_class == match[1]:
            tp += 1
        else:
            fp += 1
    fn = 19 - tp
    tn = 379 - 19 - fp
    return tp, fp, tn, fn


def precision(true_positive, false_positive):
    return true_positive / (true_positive + false_positive)


def recall(true_positive, false_negative):
    return true_positive / (true_positive + false_negative)


def accuracy(true_positive, true_negative, db_size):
    return (true_positive + true_negative) / db_size


def f1_score(pre, rec):
    if (pre + rec) == 0:
        return 0
    else:
        return 2 * ((pre * rec) / (pre + rec))


def specificity(true_negative, false_positive):
    return true_negative / (false_positive + true_negative)


def do_evalatuion_on_distances_functions(all_features):
    dist = {"euclidean": [], "cosine": [], "euc_emd": [], "euc_cos": [], "cos_emd": [], "cos_euc": [], "mixed": []}
    for f in all_features:
        q = np.array(f.create_feature_vector())
        # dist["euclidean"].append(euclidean_distance(q, all_features)[:7])
        # dist["cosine"].append(cosine_distance(q, all_features)[:7])
        # dist["euc_emd"].append(euclidean_emd(q, all_features)[:7])
        # dist["euc_cos"].append(euclidean_cosine(q, all_features)[:7])
        # dist["cos_emd"].append(cosine_emd(q, all_features)[:7])
        # dist["cos_euc"].append(cosine_euclidean(q, all_features)[:7])
        dist["mixed"].append(mixed(q, all_features)[:7])
    # print("euclidean")
    # evaluation(dist["euclidean"], all_features)
    # print("cosine")
    # evaluation(dist["cosine"], all_features)
    # print("euc_emd")
    # evaluation(dist["euc_emd"], all_features)
    # print("euc_cos")
    # evaluation(dist["euc_cos"], all_features)
    # print("cos_emd")
    # evaluation(dist["cos_emd"], all_features)
    # print("cos_euc")
    # evaluation(dist["cos_euc"], all_features)
    print("mixed")
    evaluation(dist["mixed"], all_features)


if __name__ == "__main__":
    all_features = read_feature_table()
    all_features, mean_stdv, hist_mean_stdv = normalize_standardization(all_features)
    # d = np.array([s.create_feature_vector() for s in all_features])
    # print("**Start knn**")
    # result = knn(d, all_features)
    # evaluation(result, all_features)
    # do_evalatuion_on_distances_functions(all_features)
    print("start eval")
    eval_all_combinations(all_features, False)
