import pyrender
import pyglet
import sys
import numpy as np
from pyrender.node import Node


class custom_viewer(pyrender.Viewer):
    """
    A customisation of the pyrender.Viewer class, inheriting all function from the parent "pyrender.Viewer"
    that are not explicitly overwritten.
    
    Additional parameters can be passed to the class via the __init__ method.
    Additional key commands can be added (see "pyglet.window.key.G" for an example).
    """
    def __init__(self, shapes, scene, number, vert_min_threshold):
        '''
        Method called when creating an instance of the class.
        Uses all the arguments from the "__init__" method of the parent pyrender.Viewer.
        
        :param shapes: An instance of the "shape_collection" class used to store, alter and view meshes. 
        :param scene: Scene that will be rendered; Is passed on to the parent "__init__" method.
        :param number: The position of the current mesh in the "shape_list" of the collection.
        :param vert_min_threshold: The minimum number of faces before refinement is applied. 
        '''
        self.shapes = shapes 
        self.curr_number = number 
        self.vert_min_threshold = vert_min_threshold 
        
        super().__init__(scene, use_raymond_lighting=True, render_flags={"all_wireframe":True, "all_solid":True})
        
        
    def on_key_press(self, symbol, modifiers):
        """Record a key press.
        """
        # First, check for registered key callbacks
        if symbol in self.registered_keys:
            tup = self.registered_keys[symbol]
            callback = None
            args = []
            kwargs = {}
            if not isinstance(tup, (list, tuple, np.ndarray)):
                callback = tup
            else:
                callback = tup[0]
                if len(tup) == 2:
                    args = tup[1]
                if len(tup) == 3:
                    kwargs = tup[2]
            callback(self, *args, **kwargs)
            return

        # Otherwise, use default key functions

        # A causes the frame to rotate
        self._message_text = None
        if symbol == pyglet.window.key.A:
            self.viewer_flags['rotate'] = not self.viewer_flags['rotate']
            if self.viewer_flags['rotate']:
                self._message_text = 'Rotation On'
            else:
                self._message_text = 'Rotation Off'

        # C toggles backface culling
        elif symbol == pyglet.window.key.C:
            self.render_flags['cull_faces'] = (
                not self.render_flags['cull_faces']
            )
            if self.render_flags['cull_faces']:
                self._message_text = 'Cull Faces On'
            else:
                self._message_text = 'Cull Faces Off'

        # F toggles face normals
        elif symbol == pyglet.window.key.F:
            self.viewer_flags['fullscreen'] = (
                not self.viewer_flags['fullscreen']
            )
            self.set_fullscreen(self.viewer_flags['fullscreen'])
            self.activate()
            if self.viewer_flags['fullscreen']:
                self._message_text = 'Fullscreen On'
            else:
                self._message_text = 'Fullscreen Off'

        # S toggles shadows
        elif symbol == pyglet.window.key.H and sys.platform != 'darwin':
            self.render_flags['shadows'] = not self.render_flags['shadows']
            if self.render_flags['shadows']:
                self._message_text = 'Shadows On'
            else:
                self._message_text = 'Shadows Off'

        elif symbol == pyglet.window.key.I:
            if (self.viewer_flags['show_world_axis'] and not
                    self.viewer_flags['show_mesh_axes']):
                self.viewer_flags['show_world_axis'] = False
                self.viewer_flags['show_mesh_axes'] = True
                self._set_axes(False, True)
                self._message_text = 'Mesh Axes On'
            elif (not self.viewer_flags['show_world_axis'] and
                    self.viewer_flags['show_mesh_axes']):
                self.viewer_flags['show_world_axis'] = True
                self.viewer_flags['show_mesh_axes'] = True
                self._set_axes(True, True)
                self._message_text = 'All Axes On'
            elif (self.viewer_flags['show_world_axis'] and
                    self.viewer_flags['show_mesh_axes']):
                self.viewer_flags['show_world_axis'] = False
                self.viewer_flags['show_mesh_axes'] = False
                self._set_axes(False, False)
                self._message_text = 'All Axes Off'
            else:
                self.viewer_flags['show_world_axis'] = True
                self.viewer_flags['show_mesh_axes'] = False
                self._set_axes(True, False)
                self._message_text = 'World Axis On'

        # L toggles the lighting mode
        elif symbol == pyglet.window.key.L:
            if self.viewer_flags['use_raymond_lighting']:
                self.viewer_flags['use_raymond_lighting'] = False
                self.viewer_flags['use_direct_lighting'] = True
                self._message_text = 'Direct Lighting'
            elif self.viewer_flags['use_direct_lighting']:
                self.viewer_flags['use_raymond_lighting'] = False
                self.viewer_flags['use_direct_lighting'] = False
                self._message_text = 'Default Lighting'
            else:
                self.viewer_flags['use_raymond_lighting'] = True
                self.viewer_flags['use_direct_lighting'] = False
                self._message_text = 'Raymond Lighting'

        # M toggles face normals
        elif symbol == pyglet.window.key.M:
            self.render_flags['face_normals'] = (
                not self.render_flags['face_normals']
            )
            if self.render_flags['face_normals']:
                self._message_text = 'Face Normals On'
            else:
                self._message_text = 'Face Normals Off'

        # N toggles vertex normals
        elif symbol == pyglet.window.key.N:
            self.render_flags['vertex_normals'] = (
                not self.render_flags['vertex_normals']
            )
            if self.render_flags['vertex_normals']:
                self._message_text = 'Vert Normals On'
            else:
                self._message_text = 'Vert Normals Off'

        # O toggles orthographic camera mode
        elif symbol == pyglet.window.key.O:
            self.viewer_flags['use_perspective_cam'] = (
                not self.viewer_flags['use_perspective_cam']
            )
            if self.viewer_flags['use_perspective_cam']:
                camera = self._default_persp_cam
                self._message_text = 'Perspective View'
            else:
                camera = self._default_orth_cam
                self._message_text = 'Orthographic View'

            cam_pose = self._camera_node.matrix.copy()
            cam_node = Node(matrix=cam_pose, camera=camera)
            self.scene.remove_node(self._camera_node)
            self.scene.add_node(cam_node)
            self.scene.main_camera_node = cam_node
            self._camera_node = cam_node

        # Q quits the viewer
        elif symbol == pyglet.window.key.Q:
            self.on_close()

        # R starts recording frames
        elif symbol == pyglet.window.key.R:
            if self.viewer_flags['record']:
                self.save_gif()
                self.set_caption(self.viewer_flags['window_title'])
            else:
                self.set_caption(
                    '{} (RECORDING)'.format(self.viewer_flags['window_title'])
                )
            self.viewer_flags['record'] = not self.viewer_flags['record']

        # S saves the current frame as an image
        elif symbol == pyglet.window.key.S:
            self._save_image()

        # W toggles through wireframe modes
        elif symbol == pyglet.window.key.W:
            if self.render_flags['flip_wireframe']:
                self.render_flags['flip_wireframe'] = False
                self.render_flags['all_wireframe'] = True
                self.render_flags['all_solid'] = False
                self._message_text = 'All Wireframe'
            elif self.render_flags['all_wireframe']:
                self.render_flags['flip_wireframe'] = False
                self.render_flags['all_wireframe'] = False
                self.render_flags['all_solid'] = True
                self._message_text = 'All Solid'
            elif self.render_flags['all_solid']:
                self.render_flags['flip_wireframe'] = False
                self.render_flags['all_wireframe'] = False
                self.render_flags['all_solid'] = False
                self._message_text = 'Default Wireframe'
            else:
                self.render_flags['flip_wireframe'] = True
                self.render_flags['all_wireframe'] = False
                self.render_flags['all_solid'] = False
                self._message_text = 'Flip Wireframe'

        # Z resets the camera viewpoint
        elif symbol == pyglet.window.key.Z:
            self._reset_view()
        
        ####################################
        # Beginning of custom key bindings
        ####################################
        elif symbol == pyglet.window.key.G:
            self.shapes.refine_meshes(self.vert_min_threshold)
            self.on_close()
            self.shapes.start_viewer(self.curr_number)
            # self.shapes.view_all_meshes()
        elif symbol == pyglet.window.key.J:
            self.shapes.resample_surface()
            self.on_close()
            self.shapes.start_viewer(self.curr_number)
        elif symbol == pyglet.window.key.P:
            self.shapes.align_meshes()
            self.on_close()
            self.shapes.start_viewer(self.curr_number)
        elif symbol == pyglet.window.key.B:
            self.shapes.flip_meshes()
            self.on_close()
            self.shapes.start_viewer(self.curr_number)
        ####################################
        # End of custom key bindings
        ####################################

        if self._message_text is not None:
            self._message_opac = 1.0 + self._ticks_till_fade