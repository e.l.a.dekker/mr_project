import pyrender
import numpy as np
import math
import trimesh
from quad_mesh_simplify import simplify_mesh
import open3d as o3d
import pymeshfix
import pyvista as pv

from classes.custom_viewer import custom_viewer
from classes.shape import Shape


class Shape_Collection:
    """
    Class definition containing all relevant information regarding shape collection.
    
    It stores a list of shapes that can be loaded at once. This list can the be used to
    perform calculations such as averages as well as to easily iterate over all shapes.
    """
    def __init__(self, id, show_multiple=False, calc_areas=True):
        """
        Method called when creating an instance of the class.
        
        :param id: ID of the collection.
        :param calc_areas: Boolean determining if "face_areas" list of each shape should be calculated or not.
        """
        self.classID = id  
        self.calc_areas = calc_areas
        self.viewer = custom_viewer  # Custom viewer to view the model (see "start_viewer" method).
        self.shape_list = []  # List of all shapes loaded in the collection.
        self.average_face_areas = []  # List of all calculated face areas (see "calculate_average_face_area").
        self.vert_min_threshold = 1000  # Minimum number of vertices before refinement is applied.
        self.vert_max_threshold = 5000  # Maximum  number of vertices before refinement is applied.
        self.show_multiple = show_multiple

    def __str__(self):
        return str(self.classID)
    
    def start_viewer(self, num):
        """
        Starts the viewer, rendering the mesh on position "num" in the shape_list.
        
        :param num: The position of the mesh to be viewed.
        """
        if self.show_multiple:
            self.view_all_meshes()
        else:
            mesh = pyrender.Mesh.from_trimesh(self.shape_list[num].mesh)
            scene = pyrender.Scene()
            scene.add(mesh)
            self.viewer(self, scene, num, self.vert_min_threshold)

    def view_all_meshes(self):
        """
        Start the viewer and render all meshes in one scene in a circular pattern.

        """
        scene = pyrender.Scene()

        R = (len(self.shape_list) * 5) / (2 * math.pi)
        A = (2 * math.pi) / len(self.shape_list)
        z = 0

        for i, s in enumerate(self.shape_list):
            x = math.sin(A*i)*R
            y = math.cos(A*i)*R
            m = pyrender.Mesh.from_trimesh(s.mesh)
            p = np.array([[1., 0., 0., x],
                          [0., 1., 0., y],
                          [0., 0., 1., z],
                          [0., 0., 0., 1.]])

            scene.add(m, pose=p)

        self.viewer(self, scene, 0, self.vert_min_threshold)

    def add_shape(self, shape, id, mesh_id):
        """
        Add a new shape to the collection.
        
        :param shape: The shape that should be added to the list.
        :param id: The "classID" used in the shape class.
        """
        self.shape_list.append(Shape(shape, id, mesh_id, self.calc_areas))
        
        # Optional! Takes a lot of time if used with a large number of meshes.
        # self.average_face_areas.append(self.calculate_average_face_area(self.shape_list[-1].face_areas)).

    def calculate_average_face_area(self, face_areas):
        """
        Calculates the average face area of a list of triangles.
        
        The method additionally does not support quads yet, only triangles.   
        
        :param face_areas: List of pre-calculated face areas of one mesh.
        :returns: The calculated average area.
        """
        sum = 0.0
        for t in face_areas:
            sum += t
        
        sum /= len(face_areas)
            
        return sum  
    
    def resample_surface(self): 
        '''
        A method to resample the the surface of a mesh, reducing it's number of faces.
        '''
        for shape in range(len(self.shape_list)):
            faces = np.asarray(self.shape_list[shape].mesh.faces, dtype=np.uint32())
            vertices = np.asarray(self.shape_list[shape].mesh.vertices)
            
            # if len(vertices) > self.vert_max_threshold:
            print("Current shape: ", shape)
            print("     Old number of faces: ", len(faces))
            print("     Old number of vertices: ", len(vertices))
            print("     Is_watertight: ", self.shape_list[shape].mesh.is_watertight)
            
            new_vertices, new_faces = simplify_mesh(vertices, faces, self.vert_max_threshold)
            
            self.shape_list[shape].mesh.faces = new_faces
            self.shape_list[shape].mesh.vertices = new_vertices
            
            # new_mesh = self.shape_list[shape].mesh.simplify_quadratic_decimation(self.vert_max_threshold)
            # self.shape_list[shape].mesh.faces = new_mesh
            
            print("     New number of faces: ", len(new_faces))
            print("     New number of vertices: ", len(new_vertices))
            print("     Is_watertight: ", self.shape_list[shape].mesh.is_watertight)
            
            # self.shape_list[shape].mesh.faces = new_faces
            # self.shape_list[shape].mesh.vertices = new_vertices

    

    def refine_meshes(self, vert_min_threshold):
        '''
        Loops over all faces of all meshes in the collection and applies the refinement method.
        
        Method does currently not use the "min_size" argument yet, but rather reduces the size
        of all triangles that are above average in size.
        
        The method additionally does not support quads yet, only triangles.  
        
        :param vert_min_threshold: Parameter to determine what the allowed maximum triangle size is before they are split.
        '''
        counter = 0    
        for i in range(len(self.shape_list)):
                
            avg_area = self.calculate_average_face_area(self.shape_list[i].face_areas) 

            # print("Current mesh number: ", i)
            # print("     Old average area: ", avg_area)
            # print("     Old number of faces: ", len(self.shape_list[i].mesh.faces))
                
            while len(self.shape_list[i].mesh.vertices) < vert_min_threshold:
                
                avg_area = self.calculate_average_face_area(self.shape_list[i].face_areas) 
                
                j = 0
                while j < len(self.shape_list[i].triangles):
                    if(self.shape_list[i].triangles[j]):
                        # if self.shape_list[i].face_areas[j] > max_size:
                        if self.shape_list[i].face_areas[j] > avg_area:
                            
                            self.shape_list[i].refine_mesh(j)
                            counter += 1
                            j = 0
                            
                        else:
                            j += 1
                    else:
                        #TODO: What do do with quads
                        j += 1

            # print("     ***")
            # print("     New average area: ", self.calculate_average_face_area(self.shape_list[i].face_areas))     
            # print("     New number of faces: ", len(self.shape_list[i].mesh.faces))

    def fix_normals_meshes(self):
        """Fix the normals of the meshes."""
        for shape in self.shape_list:
            shape.mesh.fix_normals()

    def align_meshes(self):
        '''
        Loops over all meshes and calls the alignment method.
        '''
        for shape in self.shape_list:
            shape.align_mesh()
            
    def flip_meshes(self):
        '''
        Loops over all meshes and calls the flipping method.
        '''
        for shape in self.shape_list:
            shape.flip_mesh()
            
    def translate__meshes(self):
        """
        Loops over all meshes and calls the translation method.
        """
        
        for shape in self.shape_list:
            shape.translate_to_barycenter()

    def scale_meshes(self):
        """
        Loops over all meshes and calls the scaling method.
        """
        for shape in self.shape_list:
            shape.scale_mesh()