import numpy as np
import math
import trimesh
import math
import pymeshlab as ms
from scipy.spatial import ConvexHull
from matplotlib import pyplot as plt
from itertools import combinations

class Shape:
    """
    Class definition containing all relevant information regarding shapes.
    """
    def __init__(self, mesh, id, mesh_id, calc_areas=True):
        """
        Method called when creating an instance of the class.
        
        :param mesh: Mesh containing the face and vertex information.
        :param id:  The class of the mesh as defined in the database.
        :param calc_areas: Boolean determining if "face_areas" list should be calculated or not.
        """
        self.classID = id
        self.meshID = mesh_id
        self.calc_areas = calc_areas
        self.mesh = mesh

        # self.num_faces = mesh.faces.shape[0]
        # self.num_vertices = mesh.vertices.shape[0]
        # self.bounding_box = mesh.bounds  # trimesh.bounds.oriented_bounds(mesh)

        t, q, fa = self.get_types_of_faces()
        self.triangles = t
        self.quads = q
        self.face_areas = fa

        self.num = 500
        self.num_2 = 320
        self.num_3 = 47
        self.num_4 = 18
        self.bins = 10
        # self.A3()
        # self.D1()
        # self.D2()
        # self.D3()
        # self.D4()

    def get_types_of_faces(self):
        """
        Get all the triangles and quads of the mesh.
        
        :returns: Masks showing which faces are triangles and quads and a list of all face areas.
        """
        triangles = []
        quads = []
        face_areas = []
        for f in self.mesh.faces:
            # print(len(f))
            if len(f) == 3:
                triangles.append(True)
                quads.append(False)
                if self.calc_areas:
                    face_areas.append(self.calculate_triangle_areas(f))
            elif len(f) == 4:
                triangles.append(False)
                quads.append(True)
                print("Quad detected!")
                # TODO
                # if self.calc_areas:
                #     face_areas.append(self.calculate_quad_area(f))
            else:
                print("N-polygon found!")

        return triangles, quads, face_areas
      
    def translate_to_barycenter(self):
        """
        Translate the barycenter of the mesh to the orgin.
        """
        translation_vec = self.mesh.centroid
        for i in range(len(self.mesh.vertices)):
            self.mesh.vertices[i] = self.mesh.vertices[i] - translation_vec

    def scale_mesh(self):
        """
        First set the min and max coordinates of the axis aligned box of unit size 1.
        Then get the min and max coordinates of the bounding box of the mesh.
        Then normalize the mesh to the box of unit size 1 by resetting the vertices of the mesh.
        """
        A = [-0.5, -0.5, -0.5]
        B = [0.5, 0.5, 0.5]
        A0 = self.mesh.bounds[0]
        B0 = self.mesh.bounds[1]

        scale = min((B[0] - A[0]) / (B0[0] - A0[0]), (B[1] - A[1]) / (B0[1] - A0[1]), (B[2] - A[2]) / (B0[2] - A0[2]))

        for v in self.mesh.vertices:
            v[0] = (v[0] - 0.5 * (A0[0] + B0[0])) * scale + 0.5 * (A[0] + B[0])
            v[1] = (v[1] - 0.5 * (A0[1] + B0[1])) * scale + 0.5 * (A[1] + B[1])
            v[2] = (v[2] - 0.5 * (A0[2] + B0[2])) * scale + 0.5 * (A[2] + B[2])

    def get_list_3_random_points(self):
        """
        Get three lists of random point. If the points are not the same add it to the list 'points'.

        :return: A list of a list of three point
        """
        points = []
        p1_r_num = np.random.choice(np.arange(len(self.mesh.vertices)), self.num_3)
        p2_r_num = np.random.choice(np.arange(len(self.mesh.vertices)), self.num_3)
        p3_r_num = np.random.choice(np.arange(len(self.mesh.vertices)), self.num_3)
        for m in p1_r_num:
            p1 = self.mesh.vertices[m]
            for n in p2_r_num:
                p2 = self.mesh.vertices[n]
                for o in p3_r_num:
                    p3 = self.mesh.vertices[o]
                    if m == n or m == o or n == o:
                        continue
                    points.append([p1, p2, p3])
        return points

    def A3(self):  # A3: angle between 3 random vertices
        """
        Get a list with three random points per entry and calculate the angle between the three point for each entry.

        :return: Histogram
        """
        all_angles = []
        p = self.get_list_3_random_points()
        for x in p:
            ba = x[0] - x[1]
            bc = x[2] - x[1]
            cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
            all_angles.append(np.degrees(np.arccos(cosine_angle)))

        weights = np.ones_like(all_angles) / float(len(all_angles))
        (n, bins, patches) = plt.hist(all_angles, bins=self.bins, range=(0.0, 180.0), weights=weights)

        return n
        # return self.plot_hist(all_angles, self.bins, 180.0,
        #                       "Frequency of the angle of three vertices",
        #                       "Angles",
        #                       "Frequency",
        #                       "../output/histograms/A3_angles_3vert_{}_{}.png".format(self.classID, self.meshID))

    def D1(self):  # D1: distance between barycenter and random vertex
        """
        Get the barycenter of the mesh and get a list of random points.
        For each point calculate the distance between the barycenter and the random vertex.

        :return: Histogram
        """
        all_distances = []
        b = self.mesh.centroid
        r_num = np.random.choice(np.arange(len(self.mesh.vertices)), self.num)
        for n in r_num:
            p = self.mesh.vertices[n]
            all_distances.append(np.linalg.norm(b - p))

        weights = np.ones_like(all_distances) / float(len(all_distances))
        (n, bins, patches) = plt.hist(all_distances, bins=self.bins, range=(0.0, math.sqrt(3)), weights=weights)

        return n
        # return self.plot_hist(all_distances, self.bins, math.sqrt(3),
        #                       "Frequency of the distance to the orgin",
        #                       "Distances",
        #                       "Frequency",
        #                       "../output/histograms/D1_dist_origin_{}_{}.png".format(self.classID, self.meshID))

    def D2(self):  # D2: distance between 2 random vertices
        """
        Get two lists of random points. For each point in the first list
        calculate the distance between it and a point in the second list with the same index.
        :return: Histogram
        """
        all_distances = []
        p1_r_num = np.random.choice(np.arange(len(self.mesh.vertices)), self.num_2)
        p2_r_num = np.random.choice(np.arange(len(self.mesh.vertices)), self.num_2)
        for n in p1_r_num:
            p1 = self.mesh.vertices[n]
            for m in p2_r_num:
                p2 = self.mesh.vertices[m]
                if n == m:
                    continue
                all_distances.append(np.linalg.norm(p1 - p2))

        weights = np.ones_like(all_distances) / float(len(all_distances))
        (n, bins, patches) = plt.hist(all_distances, bins=self.bins, range=(0.0, math.sqrt(3)), weights=weights)

        return n
        # return self.plot_hist(all_distances, self.bins, math.sqrt(3),
        #                       "Frequency of the distance between two random vertices",
        #                       "Distances",
        #                       "Frequency",
        #                       "../output/histograms/D2_dist_2verts_{}_{}.png".format(self.classID, self.meshID))

    def D3(self):  # D3: square root of area of triangle given by 3 random vertices
        """
        Get a list with three random points per entry.
        Then calculate the area of the triangle given by the 3 random vertices for each entry.

        :return: Histogram
        """
        p = self.get_list_3_random_points()
        all_areas = [math.sqrt(x) for x in trimesh.triangles.area(p)]

        weights = np.ones_like(all_areas) / float(len(all_areas))
        (n, bins, patches) = plt.hist(all_areas, bins=self.bins, range=(0.0, 1.0), weights=weights)

        return n
        # return self.plot_hist(all_areas, self.bins, 1.0,
        #                       "Frequency of the areas of three vertices",
        #                       "Square root of the area of triangles",
        #                       "Frequency",
        #                       "../output/histograms/D3_area_3vert_{}_{}.png".format(self.classID, self.meshID))

    def D4(self):  # D4: cube root of volume of tetrahedron formed by 4 random vertices
        """
        Get four lists of random numbers of length 'num'. Then create a tetrahedron with a point
        from each of the four lists at the same index. If the four points are not the same,
        calculate the volume of the tetrahedron and then take the cube root.

        :return: Histogram
        """
        all_tetrahedrons = []
        p1_r_num = np.random.choice(np.arange(len(self.mesh.vertices)), self.num_4)
        p2_r_num = np.random.choice(np.arange(len(self.mesh.vertices)), self.num_4)
        p3_r_num = np.random.choice(np.arange(len(self.mesh.vertices)), self.num_4)
        p4_r_num = np.random.choice(np.arange(len(self.mesh.vertices)), self.num_4)
        for l in p1_r_num:
            p1 = self.mesh.vertices[l]
            for m in p2_r_num:
                p2 = self.mesh.vertices[m]
                for n in p3_r_num:
                    p3 = self.mesh.vertices[n]
                    for o in p4_r_num:
                        p4 = self.mesh.vertices[o]
                        if l == m or l == n or l == o or m == n or m == o or n == o:
                            continue
                        all_tetrahedrons.append(self.cube_root(self.tetrahedron_calc_volume(p1, p2, p3, p4)))

        weights = np.ones_like(all_tetrahedrons) / float(len(all_tetrahedrons))
        (n, bins, patches) = plt.hist(all_tetrahedrons, bins=self.bins, range=(0.0, 1.0), weights=weights)

        return n
        # return self.plot_hist(all_tetrahedrons, self.bins, 1.0,
        #                       "Frequency of the volume of tetrahedrons",
        #                       "Cube root of volume of tetrahedron",
        #                       "Frequency",
        #                       "../output/histograms/D4_tetrahedrons_4vert_{}_{}.png".format(self.classID, self.meshID))

    def get_diameter(self):
        '''
        This function is caluclateing the shortest path in the mesh and then finds the largest difference in the path
        :returns: The largest difference between two points in the surface area.
        '''
        hull = ConvexHull(self.mesh.vertices)
        hull_points = self.mesh.vertices[hull.vertices, :]
        hull_points = np.array(hull_points, float)

        current_max = 0
        for a, b in combinations(np.array(hull_points), 2):
            current_distance = np.linalg.norm(a - b)
            if current_distance > current_max:
                current_max = current_distance
        return current_max

    def caluclate_features(self):
        '''
        This function is caluclateing the eccentricity,areaOfMesh,aspectRatio,diameter,volume,compactness
        :returns: List of all the features.
        '''
        areaOfMesh = self.mesh.area
        volume = math.fabs(self.mesh.volume)
        compactness = (areaOfMesh**3)/(36*math.pi*(volume**2))
        aspectRatio = self.mesh.bounding_box.volume
        rectangle = volume / math.fabs(float(self.mesh.bounding_box_oriented.volume))
        diameter = self.get_diameter()
        cov_matrix = np.cov(self.mesh.vertices.T).T
        eigenvalues, eigenvectors = np.linalg.eig(cov_matrix)
        idx = eigenvalues.argsort()[::-1]
        eigenvalues = eigenvalues[idx]
        eccentricity = math.fabs(eigenvalues[0])/math.fabs(eigenvalues[2])

        return [areaOfMesh, compactness, aspectRatio, rectangle, diameter, eccentricity]

    def cube_root(self, x):
        """
        Get the cube root of the value.
        """
        return x ** (1 / 3)

    def tetrahedron_calc_volume(self, a, b, c, d):
        """
        Calculate the volume of a tetrahedron given 4 points.

        :return: The volume
        """
        return abs(np.dot((a - b), np.cross((b - c), (c - d)))) / 6.0

    def plot_hist(self, values, bins, upper_range, hist_name, x_name, y_name, file_name):
        plt.clf()  # clear histogram
        weights = np.ones_like(values) / float(len(values))
        f, ax = plt.subplots()
        ax.set_ylim([0.0, 1.0])
        (n, bins, patches) = ax.hist(values, bins=bins, range=(0.0, upper_range), weights=weights)
        plt.title(hist_name)
        plt.xlabel(x_name)
        plt.ylabel(y_name)
        plt.savefig(file_name)
        return n

    def calculate_triangle_areas(self, triangle):
        """
        Calculate the area of a single triangle.
        
        :param triangle: The list of vertex indices describing the triangle face.
        :returns: The calculated triangle area.
        """
        # for t in test_shape.triangles:
        a = self.mesh.vertices[triangle[0]]
        b = self.mesh.vertices[triangle[1]]
        c = self.mesh.vertices[triangle[2]]

        a = trimesh.triangles.area([[a,b,c]])

        return a
    
    def calculate_quad_area(self, quad):
        # TODO
        pass
    
    def refine_mesh(self, old_id):
        """
        This method takes the ID of a single face and splits it in half.
        The current method finds the point on a random side of the triangle and
        uses it to create two smaller sub-triangles.
        
        :param old_id: The ID of the face to be split.
        """
        old_triangle = self.mesh.faces[old_id]

        a = self.mesh.vertices[old_triangle[0]]
        b = self.mesh.vertices[old_triangle[1]]
        c = self.mesh.vertices[old_triangle[2]]
        points = [a,b,c]
        point_ids = [[0,1,2], [1,2,0], [2,0,1]]
        
        max_dist = -1
        max_id = -1
        for id, ids in enumerate(point_ids):
            p1 = points[ids[0]]
            p2 = points[ids[1]]
            squared_dist = np.sum((p1-p2)**2, axis=0)
            dist = np.sqrt(squared_dist)
            if(max_dist < dist):
                max_dist = dist
                max_id = id
                
        breakpoint = []
        breakpoint.append((points[point_ids[max_id][0]][0] + points[point_ids[max_id][1]][0]) / 2)
        breakpoint.append((points[point_ids[max_id][0]][1] + points[point_ids[max_id][1]][1]) / 2)
        breakpoint.append((points[point_ids[max_id][0]][2] + points[point_ids[max_id][1]][2]) / 2)

        self.mesh.vertices = np.append(self.mesh.vertices, [breakpoint], axis=0)

        new_faces = []
        new_faces.append([old_triangle[point_ids[max_id][0]], len(self.mesh.vertices)-1, old_triangle[point_ids[max_id][2]]])
        new_faces.append([old_triangle[point_ids[max_id][2]], len(self.mesh.vertices)-1, old_triangle[point_ids[max_id][1]]])

        self.mesh.faces = np.delete(self.mesh.faces, old_id, 0)
        self.face_areas = np.delete(self.face_areas, old_id, 0)
        self.triangles = np.delete(self.triangles, old_id, 0)
        self.quads = np.delete(self.quads, old_id, 0)

        for face in new_faces:
            self.mesh.faces = np.insert(self.mesh.faces, old_id, [face], axis=0)
            self.triangles = np.insert(self.triangles, old_id, True, axis=0)
            self.quads = np.insert(self.quads, old_id, False, axis=0)
            self.face_areas = np.insert(self.face_areas, old_id, self.calculate_triangle_areas(face), axis=0)
            
    def align_mesh(self):
        """
        This method aligns the meshes so that their eigenvectors overlay with the worlds' principle axes.
        The longest eigenvector corresponds to the X axis, the second to the Y axis.
        """
        # print("Before area: ", self.mesh.area)
        # print("Before extents: ", self.mesh.extents)
        # print("Before volume: ", self.mesh.volume)
        
        cov_matrix = np.cov(self.mesh.vertices.T).T
        eigenvalues, eigenvectors = np.linalg.eig(cov_matrix)
        
        idx = eigenvalues.argsort()[::-1] 
        eigenvectors = eigenvectors[idx] 
        
        for vert in range(len(self.mesh.vertices)):
            self.mesh.vertices[vert] = [np.dot(self.mesh.vertices[vert], eigenvectors[0]), np.dot(self.mesh.vertices[vert], eigenvectors[1]), np.dot(self.mesh.vertices[vert], np.cross(eigenvectors[0], eigenvectors[1]))]
            
        # print("After area: ", self.mesh.area)
        # print("After extents: ", self.mesh.extents)
        # print("After volume: ", self.mesh.volume)

    def flip_mesh(self):      
        """
        This method flips the meshes using their moment.
        This has the effect that the side of the mesh with more vertices will be on the positive side of the axes.
        """ 
        x = 0
        y = 0
        z = 0
        for point in self.mesh.triangles_center:
            x += math.copysign(1, point[0]) * (point[0]**2)
            y += math.copysign(1, point[1]) * (point[1]**2)
            z += math.copysign(1, point[2]) * (point[2]**2)
        # print("Before flipping  >>> x: ", x, " - y: ", y, " - z: ", z)
        
        for id, val in enumerate([x,y,z]):
            if val < 0:
                for vert in range(len(self.mesh.vertices)):
                    self.mesh.vertices[vert][id] = -1*self.mesh.vertices[vert][id]

        # x = 0
        # y = 0
        # z = 0
        # for point in self.mesh.triangles_center:
        #     x += math.copysign(1, point[0]) * (point[0]**2)
        #     y += math.copysign(1, point[1]) * (point[1]**2)
        #     z += math.copysign(1, point[2]) * (point[2]**2)
        # print("After flipping >>> x: ", x, " - y: ", y, " - z: ", z)
