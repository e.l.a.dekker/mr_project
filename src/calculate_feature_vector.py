import csv
from statistics import stdev
import numpy as np
from operator import itemgetter
from scipy.stats import wasserstein_distance
from statistics import mean
from pynndescent import NNDescent
from itertools import combinations

all_features = []

# Reading the feature table from the csv
def read_feature_table():
    with open('../output/feature_extraction.csv', 'r') as file:
        features = []
        reader = csv.reader(file, delimiter=',')
        next(reader, None)
        # l = next(reader)
        # print(l[0])
        # query = make_feature(l)
        for l in reader:
            x = make_feature(l)
            features.append(x)

    return features

# Creating the feature vector
def make_feature(l):
    x = Features(int(l[0]), l[1],
                 float(l[2]), float(l[3]), float(l[4]), float(l[5]), float(l[6]), float(l[7]),
                 toList(l[8]), toList(l[9]), toList(l[10]), toList(l[11]), toList(l[12]))
    return x

# making the query from the feature vector
def make_query(l):
    x = Features(l[0], l[1], l[2], l[3], l[4], l[5], l[6], l[7], l[8], l[9], l[10], l[11], l[12])
    return x

# Function to convert to list
def toList(s):
    return [float(x) for x in s.strip('[]').split(',')]


def normalize_min_max(features):
    """
    Normalize the global descriptors of all features using min max normalization.
    Also normalize the histogram features.
    """
    hist_mean_stdv = get_mean_stdv_histogram_features(features)
    max_val, min_val = get_min_max(features)
    # normalize db
    for i, shape in enumerate(features):
        apply_min_max(max_val, min_val, shape)
        normalize_hist_features(shape, hist_mean_stdv)

    return features, max_val, min_val, hist_mean_stdv


def apply_min_max(max_val, min_val, x):
    """Apply min-max normalization on all global descriptors of the given shape."""
    x.area = (x.area - min_val[0]) / float(max_val[0] - min_val[0])
    x.compactness = (x.compactness - min_val[1]) / float(max_val[1] - min_val[1])
    x.volumeRatio = (x.volumeRatio - min_val[2]) / float(max_val[2] - min_val[2])
    x.rectangle = (x.rectangle - min_val[3]) / float(max_val[3] - min_val[3])
    x.diameter = (x.diameter - min_val[4]) / float(max_val[4] - min_val[4])
    x.eccentricity = (x.eccentricity - min_val[5]) / float(max_val[5] - min_val[5])


def get_min_max(features):
    """Get the minimum and maximum over all the global descriptors."""
    list_area = [x.area for x in features]
    list_comp = [x.compactness for x in features]
    list_volRat = [x.volumeRatio for x in features]
    list_rect = [x.rectangle for x in features]
    list_dia = [x.diameter for x in features]
    list_ecc = [x.eccentricity for x in features]
    min_val = [min(list_area), min(list_comp), min(list_volRat), min(list_rect), min(list_dia), min(list_ecc)]
    max_val = [max(list_area), max(list_comp), max(list_volRat), max(list_rect), max(list_dia), max(list_ecc)]
    return max_val, min_val


def normalize_standardization(features):
    """
    Normalize the global descriptors of all features using standardization.
    Also normalize the histogram features.
    """
    hist_mean_stdv = get_mean_stdv_histogram_features(features)
    mean_stdv_features = get_mean_stdv(features)
    # normalize db
    for i, shape in enumerate(features):
        apply_standardization(mean_stdv_features, shape)
        normalize_hist_features(shape, hist_mean_stdv)
    return features, mean_stdv_features, hist_mean_stdv


def apply_standardization(mean_stdv, shape):
    """Apply standardization on all global descriptors of the given shape."""
    shape.area = (shape.area - mean_stdv[0]) / mean_stdv[1]
    shape.compactness = (shape.compactness - mean_stdv[2]) / mean_stdv[3]
    shape.volumeRatio = (shape.volumeRatio - mean_stdv[4]) / mean_stdv[5]
    shape.rectangle = (shape.rectangle - mean_stdv[6]) / mean_stdv[7]
    shape.diameter = (shape.diameter - mean_stdv[8]) / mean_stdv[9]
    shape.eccentricity = (shape.eccentricity - mean_stdv[10]) / mean_stdv[11]

#Finding the mean standard deviation of each feature
def get_mean_stdv(features):
    mean_stdv_features = []
    list_area = [x.area for x in features]
    list_comp = [x.compactness for x in features]
    list_volRat = [x.volumeRatio for x in features]
    list_rect = [x.rectangle for x in features]
    list_dia = [x.diameter for x in features]
    list_ecc = [x.eccentricity for x in features]

    mean_stdv_features.append(float(mean(list_area)))
    mean_stdv_features.append(float(stdev(list_area)))
    mean_stdv_features.append(float(mean(list_comp)))
    mean_stdv_features.append(float(stdev(list_comp)))
    mean_stdv_features.append(float(mean(list_volRat)))
    mean_stdv_features.append(float(stdev(list_volRat)))
    mean_stdv_features.append(float(mean(list_rect)))
    mean_stdv_features.append(float(stdev(list_rect)))
    mean_stdv_features.append(float(mean(list_dia)))
    mean_stdv_features.append(float(stdev(list_dia)))
    mean_stdv_features.append(float(mean(list_ecc)))
    mean_stdv_features.append(float(stdev(list_ecc)))

    return mean_stdv_features

#Finding the mean standard deviation of histogram features
def get_mean_stdv_histogram_features(features):
    mean_stdv_features = []
    list_A3 = [v.A3 for v in features]
    list_D1 = [v.D1 for v in features]
    list_D2 = [v.D2 for v in features]
    list_D3 = [v.D3 for v in features]
    list_D4 = [v.D4 for v in features]
    mean_stdv_features.append(get_distances_combinations(list_A3))
    mean_stdv_features.append(get_distances_combinations(list_D1))
    mean_stdv_features.append(get_distances_combinations(list_D2))
    mean_stdv_features.append(get_distances_combinations(list_D3))
    mean_stdv_features.append(get_distances_combinations(list_D4))

    return mean_stdv_features

# Function to normalize the histogram features
def normalize_hist_features(shape, hist_m_s):
    shape.A3 = [(v - hist_m_s[0][0]) / hist_m_s[0][1] for v in shape.A3]
    shape.D1 = [(v - hist_m_s[1][0]) / hist_m_s[1][1] for v in shape.D1]
    shape.D2 = [(v - hist_m_s[2][0]) / hist_m_s[2][1] for v in shape.D2]
    shape.D3 = [(v - hist_m_s[3][0]) / hist_m_s[3][1] for v in shape.D3]
    shape.D4 = [(v - hist_m_s[4][0]) / hist_m_s[4][1] for v in shape.D4]

#
def get_distances_combinations(hist_list):  # 10 by len(shape)
    dist = []
    combi = [i for i in combinations(hist_list, 2)]
    for b, f in combi:
        dist.append(np.linalg.norm(np.array(b) - np.array(f)))
    return float(mean(dist)), float(stdev(dist))

#Calculating the euclidean distance for each feature
def euclidean_distance(x, features):
    distances = []
    for shape in features:
        y = np.array(shape.create_feature_vector())
        distances.append([shape.mid, shape.cid, np.linalg.norm(x - y)])
    return sorted(distances, key=itemgetter(2))

#Calculating the cosine distance for each feature
def cosine_distance(x, features):
    distances = []
    for shape in features:
        y = np.array(shape.create_feature_vector())
        distances.append([shape.mid, shape.cid, 1 - np.dot(x, y) / (np.linalg.norm(x) * np.linalg.norm(y))])
    return sorted(distances, key=itemgetter(2))

#Calculating the earth's mover distance for each feature
def euclidean_emd(x, features):
    distances = []
    for shape in features:
        y = np.array(shape.create_feature_vector())
        score = np.linalg.norm(x[:6] - y[:6]) + wasserstein_distance(x[6:], y[6:])
        distances.append([shape.mid, shape.cid, score])
    return sorted(distances, key=itemgetter(2))

# Combining cosine and emd distances
def cosine_emd(x, features):
    distances = []
    for shape in features:
        y = np.array(shape.create_feature_vector())
        score = (1 - np.dot(x[:6], y[:6]) / (np.linalg.norm(x[:6]) * np.linalg.norm(y[:6]))) + wasserstein_distance(x[6:], y[6:])
        distances.append([shape.mid, shape.cid, score])
    return sorted(distances, key=itemgetter(2))

# Combining euclidean and cosine distances
def euclidean_cosine(x, features):
    distances = []
    for shape in features:
        y = np.array(shape.create_feature_vector())
        score = np.linalg.norm(x[:6] - y[:6]) + (1 - np.dot(x[6:], y[6:]) / (np.linalg.norm(x[6:]) * np.linalg.norm(y[6:])))
        distances.append([shape.mid, shape.cid, score])
    return sorted(distances, key=itemgetter(2))

# Combining cosine and euclidean distances
def cosine_euclidean(x, features):
    distances = []
    for shape in features:
        y = np.array(shape.create_feature_vector())
        score = (1 - np.dot(x[:6], y[:6]) / (np.linalg.norm(x[:6]) * np.linalg.norm(y[:6]))) + np.linalg.norm(x[6:] - y[6:])
        distances.append([shape.mid, shape.cid, score])
    return sorted(distances, key=itemgetter(2))


def mixed(x, features):
    distances = []
    p = 6
    g = 16
    k = 26
    h = 36
    o = 46
    for shape in features:
        y = np.array(shape.create_feature_vector())
        score = euclidean(x[0:1], y[0:1]) + \
                euclidean(x[1:2], y[1:2]) + \
                euclidean(x[2:3], y[2:3]) + \
                euclidean(x[3:4], y[3:4]) + \
                euclidean(x[4:5], y[4:5]) + \
                euclidean(x[5:6], y[5:6]) + \
                cosine(x[p:g], y[p:g]) + \
                emd(x[g:k], y[g:k]) + \
                euclidean(x[k:h], y[k:h]) + \
                euclidean(x[h:o], y[h:o]) + \
                cosine(x[o:], y[o:])
        distances.append([shape.mid, shape.cid, score])
    return sorted(distances, key=itemgetter(2))

# calucalting the grid search for multiple distances
def grid_search(x, features, glob, b):
    distances = []
    for shape in features:
        score = 0
        if glob:
            score += euclidean(x.area, shape.area)
            score += euclidean(x.compactness, shape.compactness)
            score += euclidean(x.volumeRatio, shape.volumeRatio)
            score += euclidean(x.rectangle, shape.rectangle)
            score += euclidean(x.diameter, shape.diameter)
            score += euclidean(x.eccentricity, shape.eccentricity)
        else:
            score += cosine(x.area, shape.area)
            score += cosine(x.compactness, shape.compactness)
            score += cosine(x.volumeRatio, shape.volumeRatio)
            score += cosine(x.rectangle, shape.rectangle)
            score += cosine(x.diameter, shape.diameter)
            score += cosine(x.eccentricity, shape.eccentricity)
        score += choose_dist_func(b[0], np.array(x.A3), np.array(shape.A3))
        score += choose_dist_func(b[1], np.array(x.D1), np.array(shape.D1))
        score += choose_dist_func(b[2], np.array(x.D2), np.array(shape.D2))
        score += choose_dist_func(b[3], np.array(x.D3), np.array(shape.D3))
        score += choose_dist_func(b[4], np.array(x.D4), np.array(shape.D4))
        distances.append([shape.mid, shape.cid, score])
    return sorted(distances, key=itemgetter(2))[:7]


def choose_dist_func(s, x_desc, descriptor):
    if s == "euc":
        dist = euclidean(x_desc, descriptor)
    elif s == "cos":
        dist = cosine(x_desc, descriptor)
    else:
        dist = emd(x_desc, descriptor)
    return dist


def euclidean(x, y):
    return np.linalg.norm(x - y)


def cosine(x, y):
    return 1 - np.dot(x, y) / (np.linalg.norm(x) * np.linalg.norm(y))


def emd(x, y):
    return wasserstein_distance(x, y)


def knn(x, features):
    data = np.array([s.create_feature_vector() for s in features])
    index = NNDescent(data)
    index.prepare()
    k = 7
    r = index.query(x, k=k)
    all_shapes = []

    for h in range(len(r[0])):
        distances = []
        for i in range(k):
            shape_id = r[0][h][i]
            distances.append([features[shape_id].mid, features[shape_id].cid, r[1][h][i]])
        all_shapes.append(distances)
    return all_shapes


class Features:
    def __init__(self, mesh_id, cid, A, C, vR, R, D, E, A3, D1, D2, D3, D4):
        self.mid = mesh_id
        self.cid = cid
        self.area = A
        self.compactness = C
        self.volumeRatio = vR
        self.rectangle = R
        self.diameter = D
        self.eccentricity = E
        self.A3 = A3
        self.D1 = D1
        self.D2 = D2
        self.D3 = D3
        self.D4 = D4

    def print_features(self):
        print("Class:", self.cid, "Mesh:", self.mid)
        print("Area:", self.area, "Comp:", self.compactness, "volR:", self.volumeRatio, "rect", self.rectangle, "Dia:",
              self.diameter, "Ecc:", self.eccentricity)
        print("A3:", self.A3, "D1:", self.D1, "D2:", self.D2, "D3:", self.D3, "D4:", self.D4)
        print()

    def create_feature_vector(self):
        vector = [self.area, self.compactness, self.volumeRatio, self.rectangle, self.diameter, self.eccentricity]
        vector = vector + self.A3 + self.D1 + self.D2 + self.D3 + self.D4
        return vector


if __name__ == "__main__":
    norm = False
    query = [181,"Hand",0.7687773023859782,7.320862430539603,0.12131851596135591,0.21154507065759054,1.0032782956618793,23.216933050454248,"[0.19524575479423448, 0.20890361480599148, 0.15170049025745924, 0.10685493580419825, 0.08797665257216117, 0.07901909981410311, 0.06559240245417491, 0.05528640089382857, 0.036514067210540534, 0.01290658139333295]","[0.19600000000000015, 0.5280000000000004, 0.2720000000000002, 0.004, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]","[0.1590334144030321, 0.3321807757299001, 0.2893798654047286, 0.1553608579716642, 0.054805090788329305, 0.009239995702327558, 0.0, 0.0, 0.0, 0.0]","[0.15534710640223967, 0.501267726479657, 0.29846041126391154, 0.044577697654465696, 0.0003470581997319939, 0.0, 0.0, 0.0, 0.0, 0.0]","[0.6122421593437897, 0.38657886665124747, 0.0011789740050609603, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]"]
    # query1 = query
    query = make_feature(query)
    all_features = read_feature_table()
    all_features, mean_stdv, hist_mean_stdv = normalize_standardization(all_features)
    apply_standardization(mean_stdv, query)
    normalize_hist_features(query, hist_mean_stdv)

    query = np.array(query.create_feature_vector())
    # print("euclidean_distance:",euclidean_distance(query, all_features)[:6])
    # print("cosine:",cosine_distance(query, all_features))
    # print("emd:",emd(query, all_features))
    knn1 = knn(np.array([query]), all_features)[0]
    print(knn1)
    # print("knn:",knn1)
    # print("precision",evaluation(knn1,query1))
