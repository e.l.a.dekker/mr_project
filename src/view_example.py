import os
import trimesh
import pyrender
import pyglet
import numpy as np
import csv

from classes.shape_collection import Shape_Collection
from classes.custom_viewer import custom_viewer

shapes = Shape_Collection(0, show_multiple=False)  # Collection of shapes.
class_dict = {}
error_shapes = [303, 762, 888, 1041, 1043, 1049, 1055, 1080, 1083, 1087, 1120, 1377, 1541, 1693, 1755]
labels = {"Human": 1, "Cup": 21, "Glasses": 41, "Airplane": 61, "Ant": 81, "Chair": 101, "Octopus": 121,
                "Table": 141, "Teddy": 161, "Hand": 181, "Plier": 201, "Fish": 221, "Bird": 241, "Armadillo": 281,
                "Bust": 301, "Mech": 321, "Bearing": 341, "Vase": 361, "FourLeg": 381}

def getListOfFiles(dirName):
    # create a list of file and sub directories 
    # names in the given directory 
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory 
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        elif fullPath.endswith('.off'):
            allFiles.append(fullPath)
                
    return allFiles

def parse_class_file(set):
    """
    Parse the class file of the set defined in the parameter set.
    
    :param set: Defines which set to use, needs to be either 'train' or 'test'.
    :return:
    """
    folder_path = '../data/classification/v1/coarse1/'
    if set == 'train':
        file_path = folder_path + 'coarse1Train.cla'
    elif set == 'test':
        file_path = folder_path + 'coarse1Test.cla'

    curr_class = 0

    with open(file_path, "r") as f:
        next(f)
        next(f)  # Skip the first two lines
        for i, line in enumerate(f):
            if not line.strip():  # Skip empty lines
                continue
            if line[0].isdigit():
                class_dict[int(line)] = curr_class
            elif int(line.split(" ")[-1]) > 0:  # Skip empty classes
                curr_class += 1


def load_shapes(class_id=1, beginning_mesh=0, number_of_meshes=100, refine=True):
    """
    Load all meshes from the DB.
    :return:
    """
    if refine:
        file_name = 'output/remeshed_meshes'
    else:
        file_name = 'data/db'
    for i in range(class_id, class_id + 1):  # type of shape
        counter = i * 100 + beginning_mesh
        for j in range(beginning_mesh, number_of_meshes):
            if i == 18 and j == 15:
                break
            if counter not in error_shapes:
                shape = trimesh.load_mesh('../{file}/{c}/m{n}/m{n}.off'.format(file=file_name, c=i, n=counter))
                shapes.add_shape(shape, i, counter)
                print("Read mesh", counter)
            counter += 1


def load_shapes_by_set(set, refine=False):
    """
    Load all meshes from the set defined in parameter set.
    
    :param set: Defines which set to use, needs to be either 'train' or 'test'.
    :return:
    """  #
    parse_class_file(set)

    def get_digit(number):
        """
        Helper function to find the class from the file number.
        """
        return int(np.floor(number / 100))

    if refine:
        file_name = 'output/'
    else:
        file_name = 'data/db'
    for key in class_dict:
        if key != 762:
            shape = trimesh.load_mesh('../{file}/{c}/m{n}/m{n}.off'.format(file=file_name, c=get_digit(key), n=key))
            shapes.add_shape(shape, class_dict.get(key))
            print("Read mesh", key, ", class: ", class_dict.get(key))


def load_labeled_data(refine=True):
    """
    Load all meshes from the labeled PSB database.
    """
    if refine:
        file_name = '../output/fixed_watertightness'
        # file_name = '../output/preprocessed_meshes'
    else:
        file_name = '../labeled_PSB_data'

    for k, v in labels.items():  # list(labels.items())[0:1]:  # type of shape
        counter = v
        for j in range(20):
            shape = trimesh.load('{file}/{c}/{n}.off'.format(file=file_name, c=k, n=counter), process=False)
            shapes.add_shape(shape, k, counter)
            print("Read mesh", k, counter)
            counter += 1


def load_viewer():
    """
    Render a sample image.
    """
    # load_labeled_data()

    c = "Hand"
    m = 181
    # m2 = 187
    # c2 = "Glasses"
    # m3 = 41
    # c3 = "Teddy"
    # m4 = 161
    test_shape = trimesh.load('../output/fixed_watertightness/{}/{}.off'.format(c, m), process=False)
    shapes.add_shape(test_shape, c, m)
    # test_shape = trimesh.load('../output/fixed_watertightness/{}/{}.off'.format(c, m2), process=False)
    # shapes.add_shape(test_shape, c, m2)
    # test_shape = trimesh.load('../output/fixed_watertightness/{}/{}.off'.format(c2, m3), process=False)
    # shapes.add_shape(test_shape, c2, m3)
    # test_shape = trimesh.load('../output/fixed_watertightness/{}/{}.off'.format(c3, m4), process=False)
    # shapes.add_shape(test_shape, c3, m4)
    shapes.start_viewer(0)


if __name__ == "__main__":
    # load_shapes()
    # print("Done!")
    # load_viewer()
    
    # file_list = getListOfFiles()
    # i = 0
    # for id, file in enumerate(file_list): 
    #     shapes = Shape_Collection(0, show_multiple=False)  # Collection of shapes.
    #     shape = trimesh.load_mesh(file)
    #     shapes.add_shape(shape, 0)
    #     if not shapes.shape_list[-1].mesh.is_watertight:
    #         i += 1
    #         print(id)
    # print(i, " shapes were not watertight!")

    load_viewer()
