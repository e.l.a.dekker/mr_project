
import numpy as np
from sklearn.model_selection import KFold
# import skopt
# from skopt import gp_minimize
# from skopt.space import Real
# from skopt.utils import use_named_args
# from skopt.plots import plot_convergence

from calculate_feature_vector import *


# space = []
# for i in range(56):
#     space.append(Real(0.01,1, name = "cell_"+str(i)))
    
    
def calculate_weighted_euclidean_distance(x, features, weights):
    distances = []
    x = np.multiply(weights, np.array(x))
    for shape in features:
        y = np.multiply(weights, np.array(shape.create_feature_vector()))
        distances.append([shape.mid, shape.cid, np.linalg.norm(x - y)])

    return sorted(distances, key=itemgetter(2))

def weighted_cosine_distance(x, features, weights):
    distances = []
    x = np.multiply(weights, np.array(x))
    for shape in features:
        y = np.multiply(weights, np.array(shape.create_feature_vector()))
        distances.append([shape.mid, shape.cid, 1 - np.dot(x, y) / (np.linalg.norm(x) * np.linalg.norm(y))])
    return sorted(distances, key=itemgetter(2))


# def sqrt_avg_sim_loss(feat_train, feat_test, weights):
#     '''
#     Attempts to minimise the average similarity of the current split, finding the vector
#     with the lowest similarity for each feature in the test split and compared to all features
#     in the training set. The sum of these minimum similarities is then averaged for the test split,
#     which is then used for as the loss of the current weights.
#     In order to avoid the network overly reducing the similarity of few features while ignoring others,
#     the squared similarity is used to punish larger values and prevent this effect.
#
#     :param feat_train: The training split features.
#     :param feat_test: The test split features.
#     :return: The average similarity loss.
#     '''
#     losses = []
#     for feat in feat_test:
#         #dist = np.array(calculate_weighted_euclidean_distance(feat.create_feature_vector(), feat_train, weights))[:, 2].astype(np.float)
#         dist = np.array(weighted_cosine_distance(feat.create_feature_vector(), feat_train, weights))[:, 2].astype(np.float)
#
#         losses.append(np.min(np.power(dist,2)))
#
#     return np.mean(losses)
#
#
# @use_named_args(space)
# def objective(**params):
#     weights = []
#     for i in range(56):
#         weights.append(params.get("cell_"+str(i)))
#
#     split = 0
#     loss = 0
#
#     kf = KFold(n_splits=n_splits)
#     for train_index, test_index in kf.split(all_features):
#         feat_train, feat_test = np.array(all_features)[train_index], np.array(all_features)[test_index]
#         loss += sqrt_avg_sim_loss(feat_train, feat_test, weights)
#
#     return loss/n_splits
#
#
# if __name__ == "__main__":
#     n_splits = 5
#
#     all_features = read_feature_table()
#
#
#     tune_rand_gp = gp_minimize(objective,space,random_state=1234)
#     final_weights = []
#     for i in range(56):
#         final_weights.append(tune_rand_gp.x[i])
#         #final_weights.append(tune_rand_gp.get("cell_"+str(i)))
#
#     print(f"Best parameters: \n")
#     print(final_weights)
#
#     plot_convergence(tune_rand_gp)
 
# Positive loss   
# 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.3412562991731874, 0.01, 0.5584771391801308, 1.0, 0.33900705315423346, 0.49156003459663516, 1.0, 0.010001686400473378, 0.7089990564310159, 0.9146636638884748, 0.01, 0.01, 0.01, 0.01, 0.7571551376045826, 0.7338811158327099, 0.01, 0.22468923293023924, 1.0, 0.7316279435506853, 0.01, 0.01, 0.01, 0.01, 0.01, 0.6403031159583947, 1.0, 1.0, 0.30482876145044313, 0.3259981646733639, 0.01, 0.01, 0.01, 0.01, 0.01, 0.26654436616867094, 0.17241097492854787, 0.3706514918264815, 0.28141519477155574, 0.01, 0.01, 0.01, 0.01, 0.5504918022465806, 0.9510140792578935, 1.0, 0.9211314181874963, 1.0, 0.01, 0.11746892204979728
# Negative  loss
# 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.01, 0.01, 0.01, 0.01, 0.01, 1.0, 1.0, 1.0, 1.0, 0.01, 1.0, 1.0, 1.0, 0.01, 1.0, 0.01, 1.0, 1.0, 1.0, 0.0837087446930916, 1.0, 0.01, 1.0, 0.01, 0.01, 0.01, 1.0, 1.0, 1.0, 1.0, 1.0, 0.01, 1.0, 0.9742601376531339, 0.01, 1.0, 0.01, 0.01, 0.01, 1.0, 1.0, 1.0, 1.0, 0.01, 1.0, 1.0, 0.01, 0.6513169320478528, 0.01

