from view_example import load_labeled_data, shapes, labels
import pymeshfix


def preprocess_meshes():
    """Remesh and normalize all shapes in the database."""

    shapes.resample_surface()
    print("**Downsized all necessary shapes**")
    print()
    shapes.refine_meshes(shapes.vert_min_threshold)
    print("**Scaled up all necessary shapes**")
    print()
    shapes.fix_normals_meshes()
    print("**Fixed normals of all shapes**")
    print()
    shapes.translate__meshes()
    print("**Translated all shapes**")
    print()
    shapes.align_meshes()
    print("**Aligned all shapes**")
    print()
    shapes.flip_meshes()
    print("**Flipped all shapes**")
    print()
    shapes.scale_meshes()
    print("**Scaled all shapes**")
    print()

    for index_mesh, i in enumerate(shapes.shape_list):
        shapes.shape_list[index_mesh].mesh.export(
            "../output/preprocessed_meshes/{c}/{n}.off".format(c=i.classID, n=i.meshID))
        print("Exported mesh:", i.classID, i.meshID)


def fix_watertightness():
    """
    Check if the shapes are watertight. If not make the shape watertight and export the shapes again.
    """
    for index_mesh, i in enumerate(shapes.shape_list):
        if not i.mesh.is_watertight:
            vclean, fclean = pymeshfix.clean_from_arrays(i.mesh.vertices, i.mesh.faces)
            i.mesh.vertices = vclean
            i.mesh.faces = fclean
            print("Shape", i.classID, i.meshID, "is now watertight!")
        shapes.shape_list[index_mesh].mesh.export(
            "../output/fixed_watertightness/{c}/{n}.off".format(c=i.classID, n=i.meshID))
    print("**Exported all watertight meshes**")


if __name__ == "__main__":
    """
    When running the file all meshes will be remeshed, normalized and then exported ass an .off file to the folder 
    preprocessed_meshes. Next check if the meshes are watertight. If not make them watertight and export all watertight 
    meshes to the folder fixed_watertightness. 
    """
    load_labeled_data(refine=False)
    print("**Loading shapes complete**")
    print()
    preprocess_meshes()
    fix_watertightness()
