from view_example import load_labeled_data, shapes
import csv


def create_features_from_mesh(i):
    feature_extraction = [i.meshID, i.classID]
    feature_extraction = feature_extraction + i.caluclate_features()
    feature_extraction.append(list(i.A3()))
    feature_extraction.append(list(i.D1()))
    feature_extraction.append(list(i.D2()))
    feature_extraction.append(list(i.D3()))
    feature_extraction.append(list(i.D4()))

    return feature_extraction


def extract_all_features():
    with open('../output/feature_extraction.csv', 'a', newline='') as f:
        f.truncate(0)  # clear file
        writer = csv.writer(f)
        header = ['mesh','class','area','compactness','aspectRatio', 'rectangle','diameter','eccentricity','A3','D2','D1','D3','D4']
        writer.writerow(header)
        for i in shapes.shape_list:
            feature_extraction = create_features_from_mesh(i)
            writer.writerow(feature_extraction)
            print("Extracted shape:", i.classID, i.meshID)


if __name__ == "__main__":
    load_labeled_data(refine=True)
    extract_all_features()
