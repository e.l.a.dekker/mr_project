import os
from tkinter import messagebox, filedialog, ttk
import tkinter as tk
import trimesh
import numpy as np
from PIL import Image, ImageTk

from classes.shape_collection import *
from calculate_feature_vector import *
from weight_search import *
from extract_features import *

# Weights for the euclidean distance
euc_weights = [0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.3412562991731874, 0.01, 0.5584771391801308, 1.0, 0.33900705315423346, 0.49156003459663516, 1.0, 0.010001686400473378,
           0.7089990564310159, 0.9146636638884748, 0.01, 0.01, 0.01, 0.01, 0.7571551376045826, 0.7338811158327099, 0.01, 0.22468923293023924, 1.0, 0.7316279435506853,
           0.01, 0.01, 0.01, 0.01, 0.01, 0.6403031159583947, 1.0, 1.0, 0.30482876145044313, 0.3259981646733639, 0.01, 0.01, 0.01, 0.01, 0.01, 0.26654436616867094,
           0.17241097492854787, 0.3706514918264815, 0.28141519477155574, 0.01, 0.01, 0.01, 0.01, 0.5504918022465806, 0.9510140792578935, 1.0, 0.9211314181874963, 1.0, 0.01, 0.11746892204979728]

# Weights for the cosine distance
cos_weights = [0.9724877763807266, 1.0, 0.21062271978232563, 0.40045899643748667, 0.3047100553096626, 0.01, 0.6665695371362171, 0.7158435471015507, 0.21018306748150928, 0.13628024424518906,
           0.10212291931224195, 0.020397875625315823, 0.9927104954823758, 0.8096117128132427, 0.03247128477296411, 0.4477166537233898, 0.26107731159923747, 0.48350482505821385,
           0.4676917461545295, 0.841351280445363, 0.7563837466537271, 0.8281667319039786, 0.7716381629742424, 0.11524454972815988, 0.8027472139086699, 0.26561888265314704, 0.29398932823073737,
           0.08415657150163508, 0.93404469938571, 0.6336102696659855, 0.3931332526260763, 0.9902851133818382, 0.42731091163328294, 0.03307649705486463, 0.07173290013646225, 0.19795613813376697,
           0.8798083567137096, 0.6567965868224661, 0.3553685127865965, 0.23219690553090136, 0.9429580229020341, 0.12720632157481318, 0.4580363296809713, 0.9067132306845409, 0.3692594022006967,
           0.5337979908818824, 0.7625069679914734, 0.054122170347049935, 0.6489403462124834, 0.3974769492562707, 0.23878198815790033, 0.01, 0.8891252482402415, 0.862627917899218,
           0.6340536316278099, 0.540300861747725]


class Gui():
    """
    Class containing all methods regarding the GUI.
    """

    def __init__(self, norm=False, dist_func=0, weighted=False):
        self.all_features = read_feature_table()
        if norm:
            self.all_features, self.max, self.min, self.hist_mean_stdv = normalize_min_max(self.all_features)
        else:
            self.all_features, self.mean_stdv, self.hist_mean_stdv = normalize_standardization(self.all_features)

        self.dist_funstion = dist_func
        self.root = tk.Tk()
        self.root.geometry("1335x570")

        self.x_pixels = 220  # Dimensions of images
        self.y_pixels = 180

        self.frame = tk.Frame(self.root)
        self.frame.grid(row=0, column=0, padx=(10, 10), pady=(10, 10))

        # tk.Grid.rowconfigure(self.root, 2, weight=1)
        # tk.Grid.columnconfigure(self.root, 3, weight=1)

        def on_closing():
            if messagebox.askokcancel("Quit", "Do you want to quit?"):
                self.root.destroy()

        def get_model_screenshot(shape):
            scene = trimesh.Scene()
            scene.add_geometry(shape)

            png = scene.save_image(resolution=[self.x_pixels, self.y_pixels], visible=True)
            load = Image.open(trimesh.util.wrap_as_stream(png))  # .resize((self.x_pixels, self.y_pixels))
            render = ImageTk.PhotoImage(load)

            return render

        def open_file():
            self.model_path = tk.filedialog.askopenfilename(initialdir="data/db/",
                                                            filetypes=[
                                                                ("OFF files", ".off")],
                                                            title="Choose a file."
                                                            )

            if self.model_path:
                path_list = os.path.normpath(self.model_path).split(os.sep)
                model_class = path_list[-2]
                model_id = path_list[-1].split(".")[0]
                try:
                    self.shape = trimesh.load_mesh(self.model_path)
                    self.shapes = Shape_Collection(0, show_multiple=False)  # Collection of shapes.
                    self.shapes.add_shape(self.shape, model_class, int(model_id))
    
                    self.feature_extraction = create_features_from_mesh(self.shapes.shape_list[0])
                    s = make_query(self.feature_extraction)
                    if norm:
                        apply_min_max(self.max, self.min, s)
                    else:
                        apply_standardization(self.mean_stdv, s)
                    normalize_hist_features(s, self.hist_mean_stdv)
                    q = np.array(s.create_feature_vector())
                    # self.feature_extraction = np.hstack([np.array(xi) for xi in ])

                    if weighted:
                        if self.dist_funstion == 0:
                            self.dist = calculate_weighted_euclidean_distance(q, self.all_features, euc_weights)
                        else:
                            self.dist = weighted_cosine_distance(q, self.all_features, cos_weights)
                    else:
                        if self.dist_funstion == 0:
                            self.dist = euclidean_distance(q, self.all_features)
                        elif self.dist_funstion == 1:
                            self.dist = cosine_distance(q, self.all_features)
                        elif self.dist_funstion == 2:
                            self.dist = knn(np.array([q]), self.all_features)[0]
                        elif self.dist_funstion == 3:
                            self.dist = euclidean_emd(q, self.all_features)
                        elif self.dist_funstion == 4:
                            self.dist = cosine_emd(q, self.all_features)
                        elif self.dist_funstion == 5:
                            self.dist = euclidean_cosine(q, self.all_features)
                        elif self.dist_funstion == 6:
                            self.dist = cosine_euclidean(q, self.all_features)
                        else:
                            self.dist = mixed(q, self.all_features)
                    # self.shapes.add_shape(self.shape, 0)
    
                    # scene = trimesh.Scene()
                    # scene.add_geometry(self.shape)
                    #
                    # png = scene.save_image(resolution=[self.x_pixels, self.y_pixels], visible=True)
                    # load = Image.open(trimesh.util.wrap_as_stream(png))#.resize((self.x_pixels, self.y_pixels))
                    # self.render = ImageTk.PhotoImage(load)
                    self.render = get_model_screenshot(self.shape)
    
                    self.img1 = tk.Label(self.frame, image=self.render)
                    self.img1.grid(row=0, column=0, sticky="NSEW")
    
                    self.lb1 = tk.Listbox(self.frame, width=35)
                    self.lb1.insert(1, "Original Model")
                    self.lb1.insert(2, "- Class: " + model_class)
                    self.lb1.insert(3, "- Model No.: " + model_id)
                    self.lb1.grid(row=0, column=1, sticky="NSEW")
    
                    self.img_list = []
                    self.lb_list = []
                    count = 0
                    for i in range(1, 3):
                        for j in range(0, 5, 2):
                            
                            if(int(self.dist[count][0]) == int(model_id)):
                                count  += 1
                                
                            new_path = '../output/fixed_watertightness/' + self.dist[count][1] + '/' + str(
                                self.dist[count][0]) + '.off'
    
                            new_shape = trimesh.load_mesh(new_path)
                            # #self.shapes.add_shape(trimesh.load_mesh(new_path), 0, 0)
    
                            # scene = trimesh.Scene()
                            # scene.add_geometry(new_shape)
                            #
                            # png = scene.save_image(resolution=[self.x_pixels, self.y_pixels], visible=True)
                            # load = Image.open(trimesh.util.wrap_as_stream(png))#.resize((self.x_pixels, self.y_pixels))
                            # new_render = ImageTk.PhotoImage(load)
                            new_render = get_model_screenshot(new_shape)
    
                            self.img_list.append(tk.Label(self.frame, image=new_render))
                            self.img_list[-1].photo = new_render
    
                            self.img_list[-1].grid(row=i, column=j, sticky="NSEW")
                            self.lb_list.append(tk.Listbox(self.frame, width=35))
                            self.lb_list[-1].insert(1, "Number " + str(len(self.img_list)) + " similar model")
                            self.lb_list[-1].insert(2, "- Class: " + self.dist[count][1])
                            self.lb_list[-1].insert(3, "- Model No.: " + str(self.dist[count][0]))
                            self.lb_list[-1].insert(4, "- Similarity: " + str(self.dist[count][2]))
                            self.lb_list[-1].grid(row=i, column=j + 1, sticky="NSEW")
    
                            count += 1
    
                        # self.vsb = tk.ttk.Scrollbar(self.frame)
                        # self.vsb.grid(row=0, column=3,  sticky="NS", rowspan=5)
    
                        # self.vsb.config(command=self.frame.yview)

                except ValueError:
                    messagebox.showinfo("Error!", "File could not be opened")
                except FileNotFoundError:
                    messagebox.showinfo("Error!", "File Not Found")

        self.root.protocol("WM_DELETE_WINDOW", on_closing)

        self.menu = tk.Menu(self.root)
        self.root.config(menu=self.menu)

        self.file_menu = tk.Menu(self.menu, tearoff=False)
        self.file_menu.add_command(label='Open', command=open_file)

        self.menu.add_cascade(label='File', menu=self.file_menu)

        '''
        Start the main TK loop
        '''
        self.root.mainloop()


if __name__ == '__main__':
    '''
    Read in necessary files and add location names to a list
    '''
    gui = Gui(norm=False, dist_func=7, weighted=False)
    # dist_func 0 = euclidean
    #           1 = cosine
    #           2 = knn
    #           3 = euclidean for global descriptors and emd for histogram descriptors
    #           4 = cosine for global and emd for histogram
    #           5 = euclidean for global and cosine for histogram
    #           6 = cosine for global and euclidean for histogram
    #           7 = Euclidean on all separate global descriptor and D2 and D3; Cosine on A3 and D4; EMD on D1

